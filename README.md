## What to do?

---
1. Duplicate the repository into your own private bitbucket repository: 
    https://help.github.com/articles/duplicating-a-repository/
    
    Mirroring a repository:
    
    * Open Git Bash.
    * Create a bare clone of the repository.
        * git clone --bare https://bitbucket.org/igmichal/ig_qat_2019_ex.git
    * Mirror-push to the new repository.
        * cd ig_qat_2019_ex.git
        * git push --mirror https://bitbucket.org/YOUR_USER_NAME/YOUR_NEW_REPOSITORY.git
    * Remove the temporary local repository you created in step 1.
        * cd ..
        * rm -rf ig_qat_2019_ex.git
    
2. Solve exercises described in following files 
    * RestCallTest: 
    /src/master/src/test/java/exercises/ex1/RestCallTest.java
    * TriangleChecker: 
    /src/master/src/main/java/exercises/ex2/TriangleChecker.java
    /src/master/src/test/java/exercises/ex2/TriangleCheckerTest.java
    * Permutations
    /src/master/src/main/java/exercises/ex3/Permutations.java
    * Palindrome
    /src/master/src/main/java/exercises/ex4/Palindrome.java
    * Line
    /src/master/src/main/java/exercises/ex5/Line.java

3. Grant admin access to your repo to:
    * michal.banys@ig.com
    * szymon.paluchowski@ig.com
    * michal.kaczmarzyk@ig.com

4. Send email:
    michal.banys@ig.com and michal.kaczmarzyk@ig.com
    
Use Java 8, tools mentioned in tasks' description. Would be perfect if project could be compiled.
   
Please complete as many exercises as you can.

## Good luck! 