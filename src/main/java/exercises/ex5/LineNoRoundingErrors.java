package exercises.ex5;

public class LineNoRoundingErrors {

   /**
    *
    * What can be done to avoid comparing double values? Change below code to avoid rounding errors of double values
    */


   public LineNoRoundingErrors(double x0, double y0, double x1, double y1) {
      this.x0 = x0;
      this.y0 = y0;
      this.x1 = x1;
      this.y1 = y1;
   }

   // calculates the slope of the line
   public double getSlope( ) {
      if(x1 == x0) {
         throw new ArithmeticException( );
      }

      return (y1 - y0) / (x1 - x0);
   }

   // calculates the distance of the line
   public double getDistance( ) {
      return Math.sqrt(Math.pow((x1 - x0), 2) + Math.pow((y1 - y0), 2));
   }

   // returns true if a line is parallel to another
   public boolean parallelTo(LineNoRoundingErrors l) {
      // if the difference between the slopes is very small, consider them parallel
      if(Math.abs(getSlope( ) - l.getSlope( )) < .0001) {
         return true;
      } else {
         return false;
      }
   }

   private double x0, y0, x1, y1;
}
