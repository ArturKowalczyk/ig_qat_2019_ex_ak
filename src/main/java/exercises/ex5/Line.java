package exercises.ex5;

public class Line {

   /**
    * Tests for Line.java should be implemented in /src/test/java/exercises/ex5/LineTest.java
    * Create tests for the getSlope, getDistance and parallelTo methods,
    * use one of the libraries: Junit, TestNg, AssertJ.
    *
    * Because of rounding errors, it is bad practice to test double values for exact equality.
    * To get around this, you can pass a small delta value to assertEquals.
    *
    * Go to LineNoRoundingErrors.java file - there you will find further instructions
    */


   public Line(double x0, double y0, double x1, double y1) {
      this.x0 = x0;
      this.y0 = y0;
      this.x1 = x1;
      this.y1 = y1;
   }

   // calculates the slope of the line
   public double getSlope( ) {
      if(x1 == x0) {
         throw new ArithmeticException( );
      }

      return (y1 - y0) / (x1 - x0);
   }

   // calculates the distance of the line
   public double getDistance( ) {
      return Math.sqrt(Math.pow((x1 - x0), 2) + Math.pow((y1 - y0), 2));
   }

   // returns true if a line is parallel to another
   public boolean parallelTo(Line l) {
      // if the difference between the slopes is very small, consider them parallel
      if(Math.abs(getSlope( ) - l.getSlope( )) < .0001) {
         return true;
      } else {
         return false;
      }
   }

   private double x0, y0, x1, y1;
}
